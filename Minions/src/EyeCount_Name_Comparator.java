import java.util.Comparator;

public class EyeCount_Name_Comparator implements Comparator<Minion>{

	@Override
	public int compare(Minion m1, Minion m2) {
		int i;
		
		if(m1.getEyeCount() == m2.getEyeCount()) i = 0;
		else if(m1.getEyeCount() == 1) i = 1; // m1 should be after m2
		else if (m2.getEyeCount() == 1) i = -1; // m2 should be after m1
		else i =  m1.getEyeCount() - m2.getEyeCount();
		
		if (i!=0) return i;
		
		// if they have same # eyes, compare by name (descending order)
		return m2.getName().compareTo(m1.getName());
	}

}
