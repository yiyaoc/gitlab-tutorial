import java.util.Objects;

public class Minion {
	private String name;
	private int eyeCount;
	private String item;

	public Minion(String name, int eyeCount, String item) {
		this.name = name;
		this.eyeCount = eyeCount;
		this.item = item;
	}
	
	public String getName() {
		return name;
	}

	public int getEyeCount() {
		return eyeCount;
	}

	public String getItem() {
		return item;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(eyeCount, item, name);
	}

	// return true if Minion-objects have the same name, eyeCount and item
	// they will be treated as the same object when inserting into Set
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Minion other = (Minion) obj;
		return eyeCount == other.eyeCount && Objects.equals(item, other.item) && Objects.equals(name, other.name);
	}

	public String toString() {
		return name + " " + eyeCount + " " + item;
	}


}
