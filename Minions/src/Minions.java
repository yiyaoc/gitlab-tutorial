import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Minions {
	private static Set<Minion> minions = new HashSet<>(); // Minion-objects without duplicates

	public static void main(String[] args) {

		if (args.length != 2) {
			System.out.println("Proper Usage: java Minions <input file> <output file>");
			System.exit(0);
		}

		readFile(args[0]);

		oneEyedMinions_AscendingOrder(minions);

		List<Minion> sorted = sortDescending_oneEyedLast();
		
		writeToFile(sorted, args[0], args[1]);

	}

	private static void readFile(String filePath) {

		// for each line, split it by comma, create Minion-object
		// then collect all Minion-objects to a Set
		try (Stream<String> lines = Files.lines(Path.of(filePath))) {

			minions = lines.skip(1).map(line -> {
				String[] arr = line.split(",");
				return new Minion(
						arr[0],
						Integer.parseInt(arr[1]),
						arr[2]);
			}).collect(Collectors.toSet()); // output to set to avoid duplicates

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unused")
	private static String getFileExtension(String filepath) {
		String extension = "";
		int i = filepath.lastIndexOf("."); // return position of last occurrence of .

		if (i != -1) {
			extension = filepath.substring(i);
		}

		return extension;
	}

	// print one-eyed minions in ascending alphabetical order
	public static void oneEyedMinions_AscendingOrder(Set<Minion> minions) {

		System.out.println("Minions with one eye, ascending order:\n");
		minions.stream()
				.sorted(Comparator.comparing(Minion::getName))
				.filter(m -> m.getEyeCount() == 1)
				.forEach(m -> System.out.println(m));
	}
	
	/*
	// sort all minions in descending alphabetical order
	// one-eyed minions comes last in the sorting
	public static List<Minion> sortDescending_oneEyedLast(List<Minion> minionsAscending) {

		List<Minion> sorted =  new ArrayList<>(minionsAscending); // copy the sorted list
		Collections.reverse(sorted); // get minions in descending order

		int i = 0;  // increment by 1 in each iteration, while-loop ends when size of list is reached
		int j = 0; // keep track of indices

		// if the minion has 1 eye
		// take it out and append it at the end of the list
		while (i < sorted.size()) {
			if (sorted.get(j).getEyeCount() == 1) {
				Minion removed = sorted.remove(j);
				sorted.add(removed);

				// not incrementing j here because
				// after removal, the empty position will be taken by the next new element in the list
			} else {
				j++; // increment j only if eyeCount != 1, this means we can check the next Minion-object
			}
			i++;
		}

		return sorted;
	}
	*/
	
	public static List<Minion> sortDescending_oneEyedLast() {

		List<Minion> minionList =  new ArrayList<>(minions); 
		
		Collections.sort(minionList, new EyeCount_Name_Comparator());
		
		System.out.println("\nsortDescending_oneEyedLast()\n");

		for (Minion m : minionList) {
			System.out.println(m);
		}
		
		return minionList;
	}
	

	// write minions in descending order to output file
	private static void writeToFile(List<Minion> content, String inputfile, String outputfile) {

		Path path = Paths.get(outputfile);

		try {
			Files.deleteIfExists(path); // avoid FileAlreadyExistsException
			Files.createFile(path);

			// write header
			Files.writeString(path, "Name, # of eyes, Item\n", StandardOpenOption.APPEND);

			// write content
			for (Minion m : content) {
				String line = m.getName() + ", " + m.getEyeCount() + ", " + m.getItem() + "\n";
				Files.writeString(path, line, StandardOpenOption.APPEND);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
